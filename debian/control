Source: monkeysign
Section: utils
Priority: optional
Maintainer: Antoine Beaupré <anarcat@debian.org>
Build-Depends: debhelper (>= 9)
             , python (>= 2.6)
             , python-setuptools
             , python-qrencode
             , python-gtk2
             , python-zbar
             , python-zbarpygtk
             , python-socks
             , python-docutils
             , python-sphinx
             , python-sphinx-rtd-theme
             , dh-python (>= 2.20160609~)
             , python-setuptools-scm
             , python-mock
             , gnupg
             , gnupg-agent
             , dirmngr
XS-Python-Version: >= 2.7
Standards-Version: 3.9.8
Homepage: http://web.monkeysphere.info/monkeysign
Vcs-Browser: https://0xACAB.org/monkeysphere/monkeysign
Vcs-Git: https://0xACAB.org/monkeysphere/monkeysign.git

Package: monkeysign
Architecture: all
Depends: ${misc:Depends}
       , ${python:Depends}
       , python-pkg-resources
       , python-socks
       , gnupg
       , gnupg-agent
       , dirmngr
Recommends: python-qrencode
          , python-gtk2
          , python-zbar
          , python-zbarpygtk
          , python-mock
Suggests: monkeysign-doc
Description: OpenPGP key signing and exchange for humans
 monkeysign is a project to overhaul the OpenPGP keysigning experience
 and bring it closer to something that most primates can understand.
 .
 To achieve this goal, it makes use of cheap digital cameras and the
 type of bar code known as a QRcode to provide a human-friendly yet
 still-secure keysigning experience.
 .
 No more reciting tedious strings of hexadecimal characters.  And, you
 can build a little rogue's gallery of the people that you have met
 and exchanged keys with!
 .
 Monkeysign is also the name of it's commandline signing software, a
 caff replacement. Monkeyscan is the graphical user interface that
 scans qrcodes.

Package: monkeysign-doc
Architecture: all
Depends: ${misc:Depends}
       , ${sphinxdoc:Depends}
       , python-sphinx-rtd-theme
Replaces: monkeysign (<< 2.1.0)
Breaks: monkeysign (<< 2.1.0)
Section: doc
Description: OpenPGP key signing and exchange for humans (documentation)
 monkeysign is a project to overhaul the OpenPGP keysigning experience
 and bring it closer to something that most primates can understand.
 .
 To achieve this goal, it makes use of cheap digital cameras and the
 type of bar code known as a QRcode to provide a human-friendly yet
 still-secure keysigning experience.
 .
 No more reciting tedious strings of hexadecimal characters.  And, you
 can build a little rogue's gallery of the people that you have met
 and exchanged keys with!
 .
 Monkeysign is also the name of it's commandline signing software, a
 caff replacement. Monkeyscan is the graphical user interface that
 scans qrcodes.
 .
 This package ships with the builtin documentation.
