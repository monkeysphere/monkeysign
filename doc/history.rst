History
=======

A first prototype of Monkeysign was created during the 2010 `The Next
HOPE <http://www.thenexthope.org/>`_ in New York City by Jérôme
Charaoui, after a discussion with Daniel Kahn Gillmor and Antoine
Beaupré.

During the `following HOPE conference <http://hopenumbernine.net/>`_
in 2012, the happy group met again and this time Antoine pushed the
project much further. On the train ride back home, he implementing a
complete GnuPG compatibility layer that imported keys, signed UIDs and
so on, unaware of the existence of the `python-gnupg` project. This
led to the publication of the first 0.1 release, which was already a
good ``caff`` replacement, with a GUI and qr-code support.

After one more year of development and testing, Antoine released the
first stable 1.0 release in 2013 with SMTP support, unitests and most
features we now take for granted in Monkeysign. The 1.x branch was
fairly short-lived, a 2.0.0 release was published in 2014 with
simplified interface, image files support and more improvements. This
release was the first long term support branch, issued to coincide
with the Debian Jessie release.

Since then, a 2.1.x series was published to support GnuPG 2.1, and
2.2.x was released with support for Tor.

See :ref:`schedule` for more information about supported branches.

Detailed changelog
------------------

Here is the complete historical record of all known Monkeysign
releases at the time of writing. More details about changes is also
available in the git repository.

.. include:: ../debian/changelog
   :literal:
